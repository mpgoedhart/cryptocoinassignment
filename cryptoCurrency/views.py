import requests
from django.shortcuts import render
from cryptoCurrency.models import CryptoCoinAsset


def index(request):

    # Todo: This API call needs to run as an scheduled task.
    # Using coincap API
    # https://docs.coincap.io/
    url = 'http://api.coincap.io/v2/assets'
    response = requests.get(url)
    __json = response.json()
    _list = __json['data']

    for item in _list:
        temp = CryptoCoinAsset()
        temp.name = item['name']
        temp.id = item['id']
        temp.rank = item['rank']
        temp.maxSupply = item['maxSupply']
        temp.priceUsd = item['priceUsd']
        temp.percChange = item['changePercent24Hr']
        temp.save()



    return render(request, 'index.html')
