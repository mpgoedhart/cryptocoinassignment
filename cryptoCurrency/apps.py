from django.apps import AppConfig


class CryptocurrencyConfig(AppConfig):
    name = 'cryptoCurrency'
