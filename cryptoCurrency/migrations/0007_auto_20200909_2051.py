# Generated by Django 3.0.3 on 2020-09-09 18:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cryptoCurrency', '0006_auto_20200909_2048'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cryptocoinasset',
            name='maxSupply',
            field=models.DecimalField(decimal_places=2, default=-1, max_digits=1000, null=True),
        ),
    ]
