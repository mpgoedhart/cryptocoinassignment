from django.db import models


class CryptoCoinAsset(models.Model):
    id = models.TextField(primary_key=True)
    rank = models.IntegerField(default=-1)
    name = models.TextField(default=-1)
    maxSupply = models.DecimalField(default=-1, decimal_places=2, max_digits=1000, null=True)
    priceUsd = models.DecimalField(default=-1, decimal_places=2, max_digits=1000)
    percChange = models.DecimalField(default=0, decimal_places=2, max_digits=1000)


